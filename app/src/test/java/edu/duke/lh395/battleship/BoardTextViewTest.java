package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest { 
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27, 'X');
        // you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
      }

    @Test
    public void test_display_empty_2by2() {
        Board<Character> b1 = new BattleShipBoard<Character>(2, 2, 'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader +
                "A  |  A\n" +
                "B  |  B\n" +
                expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_3by2() {
        String header = "  0|1|2\n";
        String body =   "A  | |  A\n" +
                        "B  | |  B\n";
        emptyBoardHelper(3, 2, header, body);
    }

    @Test
    public void test_display_empty_3by5() {
        String header = "  0|1|2\n";
        String body =   "A  | |  A\n" +
                        "B  | |  B\n" +
                        "C  | |  C\n" +
                        "D  | |  D\n" +
                        "E  | |  E\n";
        emptyBoardHelper(3, 5, header, body);
    }

    @Test
    public void test_add_ships() {
        Board<Character> b = new BattleShipBoard<Character>(9, 20, 'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(0, 0), 's', '*');
        RectangleShip<Character> s2 = new RectangleShip<Character>(new Coordinate(0, 1), 's', '*');
        RectangleShip<Character> s3 = new RectangleShip<Character>(new Coordinate(0, 2), 's', '*');

        assertEquals(null, b.tryAddShip(s1));
        assertEquals(null, b.tryAddShip(s2));
        assertEquals(null, b.tryAddShip(s3));

        BoardTextView view = new BoardTextView(b);
        String expectedHeader = "  0|1|2|3|4|5|6|7|8\n";
        String expectedBody =   "A s|s|s| | | | | |  A\n" +
                                "B  | | | | | | | |  B\n" +
                                "C  | | | | | | | |  C\n" +
                                "D  | | | | | | | |  D\n" +
                                "E  | | | | | | | |  E\n" +
                                "F  | | | | | | | |  F\n" +
                                "G  | | | | | | | |  G\n" +
                                "H  | | | | | | | |  H\n" +
                                "I  | | | | | | | |  I\n" +
                                "J  | | | | | | | |  J\n" +
                                "K  | | | | | | | |  K\n" +
                                "L  | | | | | | | |  L\n" +
                                "M  | | | | | | | |  M\n" +
                                "N  | | | | | | | |  N\n" +
                                "O  | | | | | | | |  O\n" +
                                "P  | | | | | | | |  P\n" +
                                "Q  | | | | | | | |  Q\n" +
                                "R  | | | | | | | |  R\n" +
                                "S  | | | | | | | |  S\n" +
                                "T  | | | | | | | |  T\n";
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    private Board<Character> createBoardWithShip(int width, int height, char missInfo, Placement placement) {
        Board<Character> b = new BattleShipBoard<Character>(width, height, missInfo);
        V1ShipFactory f = new V1ShipFactory();
        Ship<Character> s = f.makeSubmarine(placement);
        b.tryAddShip(s);
        return b;
    }

    @Test
    public void test_display_my_board_with_enemy_next_to_it() {
        Board<Character> b1 = createBoardWithShip(4, 3, 'X', new Placement(new Coordinate(0, 0), 'V'));
        BoardTextView view1 = new BoardTextView(b1);
        Board<Character> b2 = createBoardWithShip(4, 3, 'X', new Placement(new Coordinate(0, 2), 'V'));
        BoardTextView view2 = new BoardTextView(b2);
        b1.fireAt(new Coordinate("a0"));
        b2.fireAt(new Coordinate("a0"));
        String expectedView1 = "     Your Ocean               Player B's Ocean\n" +
                                "  0|1|2|3                    0|1|2|3\n" +
                                "A *| | |  A                A X| | |  A\n" +
                                "B s| | |  B                B  | | |  B\n" +
                                "C  | | |  C                C  | | |  C\n" +
                                "  0|1|2|3                    0|1|2|3\n";
        String expectedView2 = "     Your Ocean               Player A's Ocean\n" +
                                "  0|1|2|3                    0|1|2|3\n" +
                                "A  | |s|  A                A s| | |  A\n" +
                                "B  | |s|  B                B  | | |  B\n" +
                                "C  | | |  C                C  | | |  C\n" +
                                "  0|1|2|3                    0|1|2|3\n";
        assertEquals(expectedView1, view1.displayMyBoardWithEnemyNextToIt(view2, "Your Ocean", "Player B's Ocean"));
        assertEquals(expectedView2, view2.displayMyBoardWithEnemyNextToIt(view1, "Your Ocean", "Player A's Ocean"));
    }
}