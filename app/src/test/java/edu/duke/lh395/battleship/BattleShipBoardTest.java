package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
    for (int i = 0; i < expected.length; i++) {
      for (int j = 0; j < expected[i].length; j++) {
        assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
      }
    }
  }

  @Test
  public void test_empty_board() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(9, 20, 'X');
    Character[][] expected = new Character[20][9];
    for (int i = 0; i < expected.length; i++) {
      for (int j = 0; j < expected[i].length; j++) {
        expected[i][j] = null;
      }
    }
    checkWhatIsAtBoard(b, expected);
  }

  @Test
  public void test_add_ships() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(9, 20, 'X');
    RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(0, 0), 's', '*');
    RectangleShip<Character> s2 = new RectangleShip<Character>(new Coordinate(0, 1), 's', '*');
    RectangleShip<Character> s3 = new RectangleShip<Character>(new Coordinate(0, 2), 's', '*');

    assertEquals(null, b.tryAddShip(s1));
    assertEquals(null, b.tryAddShip(s2));
    assertEquals(null, b.tryAddShip(s3));

    Character[][] expected = new Character[20][9];
    for (int i = 0; i < expected.length; i++) {
      for (int j = 0; j < expected[i].length; j++) {
        expected[i][j] = null;
      }
    }
    expected[0][0] = 's';
    expected[0][1] = 's';
    expected[0][2] = 's';
    checkWhatIsAtBoard(b, expected);
  }

  @Test
  public void test_fire_at() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(9, 20, 'X');
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s1 = f.makeSubmarine(new Placement("A0V"));
    Ship<Character> s2 = f.makeDestroyer(new Placement("d0h"));
    assertNull(b.tryAddShip(s1));
    assertNull(b.tryAddShip(s2));
    assertEquals(null, b.whatIsAtForEnemy(new Coordinate("A0")));
    assertEquals('s', b.whatIsAtForSelf(new Coordinate("a0")));
    assertEquals('s', b.whatIsAtForSelf(new Coordinate("B0")));
    assertSame(s1, b.fireAt(new Coordinate("a0")));
    assertEquals('s', b.whatIsAtForEnemy(new Coordinate("a0")));
    assertEquals('*', b.whatIsAtForSelf(new Coordinate("a0")));
    assertFalse(s1.isSunk());
    b.fireAt(new Coordinate("b0"));
    assertTrue(s1.isSunk());

    assertNull(b.fireAt(new Coordinate("a1")));
    assertNull(b.fireAt(new Coordinate("g0")));
    assertEquals('X', b.whatIsAtForEnemy(new Coordinate("A1")));
    assertEquals('X', b.whatIsAtForEnemy(new Coordinate("g0")));
  }

  @Test
  public void test_all_sunk() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(9, 20, 'X');
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s1 = f.makeSubmarine(new Placement("A0V"));
    Ship<Character> s2 = f.makeDestroyer(new Placement("d0h"));
    Ship<Character> s3 = f.makeBattleship(new Placement("g0h"));
    Ship<Character> s4 = f.makeCarrier(new Placement("j0v"));
    assertNull(b.tryAddShip(s1));
    assertNull(b.tryAddShip(s2));
    assertNull(b.tryAddShip(s3));
    assertNull(b.tryAddShip(s4));
    assertFalse(b.allSunk());
    b.fireAt(new Coordinate("a0"));
    b.fireAt(new Coordinate("b0"));
    assertFalse(b.allSunk());
    b.fireAt(new Coordinate("d0"));
    b.fireAt(new Coordinate("d1"));
    b.fireAt(new Coordinate("d2"));
    b.fireAt(new Coordinate("g0"));
    b.fireAt(new Coordinate("g1"));
    b.fireAt(new Coordinate("g2"));
    b.fireAt(new Coordinate("g3"));
    b.fireAt(new Coordinate("j0"));
    b.fireAt(new Coordinate("k0"));
    b.fireAt(new Coordinate("l0"));
    b.fireAt(new Coordinate("m0"));
    b.fireAt(new Coordinate("n0"));
    b.fireAt(new Coordinate("o0"));
    assertTrue(b.allSunk());
  }
}