package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
    @Test
    public void test_no_collision_rule_checker() {
        V1ShipFactory f = new V1ShipFactory();
        NoCollisionRuleChecker<Character> noCollisionChecker = new NoCollisionRuleChecker<Character>(null);
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, noCollisionChecker, 'X');
        Ship<Character> s1 = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
        Ship<Character> s2 = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'V'));
        Ship<Character> s3 = f.makeSubmarine(new Placement(new Coordinate(1, 0), 'H'));
        b.tryAddShip(s1);
        assertEquals("That placement is invalid: the ship overlaps another ship.", noCollisionChecker.checkMyRule(s2, b));
        assertEquals(null, noCollisionChecker.checkMyRule(s3, b));
    }

    @Test
    public void test_no_collision_and_inbounds() {
        V1ShipFactory f = new V1ShipFactory();
        NoCollisionRuleChecker<Character> noCollisionChecker = new NoCollisionRuleChecker<Character>(null);
        InBoundsRuleChecker<Character> inboundsChecker = new InBoundsRuleChecker<Character>(noCollisionChecker);
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, inboundsChecker, 'X');
        Ship<Character> s1 = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
        Ship<Character> s2 = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'V'));
        Ship<Character> s3 = f.makeSubmarine(new Placement(new Coordinate(1, 0), 'H'));
        Ship<Character> s4 = f.makeSubmarine(new Placement(new Coordinate(9, 9), 'V'));
        Ship<Character> s5 = f.makeSubmarine(new Placement(new Coordinate(9, 9), 'V'));
        Ship<Character> s6 = f.makeSubmarine(new Placement(new Coordinate(9, 8), 'H'));
        b.tryAddShip(s1);
        assertEquals("That placement is invalid: the ship overlaps another ship.", noCollisionChecker.checkMyRule(s2, b));
        assertEquals(null, noCollisionChecker.checkMyRule(s3, b));
        assertEquals(null, inboundsChecker.checkMyRule(s2, b));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", inboundsChecker.checkMyRule(s4, b));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", inboundsChecker.checkMyRule(s5, b));
        assertEquals(null, inboundsChecker.checkMyRule(s6, b));
    }
}
