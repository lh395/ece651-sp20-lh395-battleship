package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Placement p1 = new Placement(c1, 'V');
        Placement p2 = new Placement(c2, 'v');
        assertEquals(p1, p2);
    }

    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(0, 3);
        Coordinate c4 = new Coordinate(2, 1);
        Placement p1 = new Placement(c1, 'V');
        Placement p2 = new Placement(c2, 'v');
        Placement p3 = new Placement(c3, 'H');
        Placement p4 = new Placement(c4, 'h');
        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }

    @Test
    void test_field_constructor_valid_cases() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(0, 3);
        Placement p1 = new Placement(c1, 'V');
        Placement p2 = new Placement(c2, 'v');
        Placement p3 = new Placement(c3, 'H');
        assertEquals(p1.getCoordinate(), c1);
        assertEquals(p2.getOrientation(), 'V');
        assertEquals(p3.getOrientation(), 'H');
    }

    @Test
    void test_field_constructor_error_cases() {
        Coordinate c1 = new Coordinate(1, 2);
        assertThrows(IllegalArgumentException.class, () -> new Placement(c1, 'G'));
        assertThrows(IllegalArgumentException.class, () -> new Placement(c1, '2'));
    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3V");
        assertEquals(new Coordinate("B3"), p1.getCoordinate());
        assertEquals('V', p1.getOrientation());
        Placement p2 = new Placement("D5H");
        assertEquals(new Coordinate("D5"), p2.getCoordinate());
        assertEquals('H', p2.getOrientation());
        Placement p3 = new Placement("a9v");
        assertEquals(new Coordinate("A9"), p3.getCoordinate());
        assertEquals('V', p3.getOrientation());
    }

    @Test
    void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("000"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0B"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[054"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A:O"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
    }
}
