package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.IOException;

public class TextPlayerTest {
    private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

    @Test
    public void test_readPlacement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
        String prompt = "Where would you like to put your ship?";
        Placement[] expected = new Placement[] { new Placement("B2V"), new Placement("C8H"), new Placement("a4v") };
        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(expected[i], p);
            assertEquals(prompt + "\n", bytes.toString());
            bytes.reset();
        }
    }

    @Test
    public void test_readCoordinate() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2\nC8\na4\n", bytes);
        String prompt = "Where would you like to put your ship?";
        Coordinate[] expected = new Coordinate[] { new Coordinate("B2"), new Coordinate("C8"), new Coordinate("a4") };
        for (int i = 0; i < expected.length; i++) {
            Coordinate c = player.readCoordinate(prompt);
            assertEquals(expected[i], c);
            assertEquals(prompt + "\n", bytes.toString());
            bytes.reset();
        }
    }

    @Test
    public void test_play_one_round() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(10, 20, "B2\n", bytes);
        TextPlayer player2 = createTextPlayer(10, 20, "A2\n", bytes);
        V1ShipFactory f = new V1ShipFactory();
        player2.getTheBoard().tryAddShip(f.makeSubmarine(new Placement("A2V")));
        player1.playOneTurn(player2.getTheBoard(), player2.getName(), false);
        String expected = "You hit a submarine!";
        assertEquals(expected, bytes.toString().split("\n")[bytes.toString().split("\n").length - 1]);
    }
}
