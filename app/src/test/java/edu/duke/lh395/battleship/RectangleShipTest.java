package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
    @Test
    public void test_makeCoords() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        HashSet<Coordinate> coordinates = RectangleShip.makeCoords(upperLeft, width, height);
        assertEquals(6, coordinates.size());
        assertTrue(coordinates.contains(new Coordinate(1, 1)));
        assertTrue(coordinates.contains(new Coordinate(1, 2)));
        assertTrue(coordinates.contains(new Coordinate(2, 1)));
        assertTrue(coordinates.contains(new Coordinate(2, 2)));
        assertTrue(coordinates.contains(new Coordinate(3, 1)));
        assertTrue(coordinates.contains(new Coordinate(3, 2)));
    }

    @Test
    public void test_RectangleShip() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        char data = 's';
        char onHit = '*';
        RectangleShip<Character> ship = new RectangleShip<Character>("submarine", upperLeft, width, height, data, onHit);
        assertEquals("submarine", ship.getName());
        assertEquals(6, ship.myPieces.size());
        assertTrue(ship.myPieces.containsKey(new Coordinate(1, 1)));
        assertTrue(ship.myPieces.containsKey(new Coordinate(1, 2)));
        assertTrue(ship.myPieces.containsKey(new Coordinate(2, 1)));
        assertTrue(ship.myPieces.containsKey(new Coordinate(2, 2)));
        assertTrue(ship.myPieces.containsKey(new Coordinate(3, 1)));
        assertTrue(ship.myPieces.containsKey(new Coordinate(3, 2)));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
    }

    @Test
    public void test_isSunk() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        char data = 's';
        char onHit = '*';
        RectangleShip<Character> ship = new RectangleShip<Character>("submarine", upperLeft, width, height, data, onHit);
        assertEquals("submarine", ship.getName());
        assertEquals(false, ship.isSunk());
        ship.recordHitAt(new Coordinate(1, 1));
        assertEquals(false, ship.isSunk());
        ship.recordHitAt(new Coordinate(1, 2));
        assertEquals(false, ship.isSunk());
        ship.recordHitAt(new Coordinate(2, 1));
        assertEquals(false, ship.isSunk());
        ship.recordHitAt(new Coordinate(2, 2));
        assertEquals(false, ship.isSunk());
        ship.recordHitAt(new Coordinate(3, 1));
        assertEquals(false, ship.isSunk());
        ship.recordHitAt(new Coordinate(3, 2));
        assertEquals(true, ship.isSunk());
    }

    @Test
    public void test_recordHitAt() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        char data = 's';
        char onHit = '*';
        RectangleShip<Character> ship = new RectangleShip<Character>("submarine", upperLeft, width, height, data, onHit);
        assertEquals("submarine", ship.getName());
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
        ship.recordHitAt(new Coordinate(1, 1));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
        ship.recordHitAt(new Coordinate(2, 1));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
        ship.recordHitAt(new Coordinate(3, 1));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
    }

    @Test
    public void test_wasHitAt() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        char data = 's';
        char onHit = '*';
        RectangleShip<Character> ship = new RectangleShip<Character>("submarine", upperLeft, width, height, data, onHit);
        assertEquals("submarine", ship.getName());
        assertEquals(false, ship.wasHitAt(new Coordinate(1, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(1, 2)));
        assertEquals(false, ship.wasHitAt(new Coordinate(2, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(2, 2)));
        assertEquals(false, ship.wasHitAt(new Coordinate(3, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(3, 2)));
        ship.recordHitAt(new Coordinate(1, 1));
        assertEquals(true, ship.wasHitAt(new Coordinate(1, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(1, 2)));
        assertEquals(false, ship.wasHitAt(new Coordinate(2, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(2, 2)));
        assertEquals(false, ship.wasHitAt(new Coordinate(3, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(3, 2)));
        ship.recordHitAt(new Coordinate(2, 1));
        assertEquals(true, ship.wasHitAt(new Coordinate(1, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(1, 2)));
        assertEquals(true, ship.wasHitAt(new Coordinate(2, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(2, 2)));
        assertEquals(false, ship.wasHitAt(new Coordinate(3, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(3, 2)));
        ship.recordHitAt(new Coordinate(3, 1));
        assertEquals(true, ship.wasHitAt(new Coordinate(1, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(1, 2)));
        assertEquals(true, ship.wasHitAt(new Coordinate(2, 1)));
        assertEquals(false, ship.wasHitAt(new Coordinate(2, 2)));
        assertEquals(true, ship.wasHitAt(new Coordinate(3, 1)));
    }

    @Test
    public void test_getDisplayInfoAt() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        char data = 's';
        char onHit = '*';
        RectangleShip<Character> ship = new RectangleShip<Character>("submarine", upperLeft, width, height, data, onHit);
        assertEquals("submarine", ship.getName());
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
        ship.recordHitAt(new Coordinate(1, 1));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
        ship.recordHitAt(new Coordinate(2, 1));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(3, 2), true));
        ship.recordHitAt(new Coordinate(3, 1));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(1, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(2, 1), true));
        assertEquals(data, ship.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals(onHit, ship.getDisplayInfoAt(new Coordinate(3, 1), true));
    }

    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLocs[0], true));
        for (Coordinate c : expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
        }
    }

    @Test
    public void test_createShip() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        V1ShipFactory factory = new V1ShipFactory();
        Ship<Character> v_sbm = factory.makeSubmarine(v1_2);
        Ship<Character> h_sbm = factory.makeSubmarine(h1_2);
        checkShip(v_sbm, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));
        checkShip(h_sbm, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));
        Ship<Character> v_dsr = factory.makeDestroyer(v1_2);
        Ship<Character> h_dsr = factory.makeDestroyer(h1_2);
        checkShip(v_dsr, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
        checkShip(h_dsr, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
        Ship<Character> v_btl = factory.makeBattleship(v1_2);
        Ship<Character> h_btl = factory.makeBattleship(h1_2);
        checkShip(v_btl, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));
        checkShip(h_btl, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5));
        Ship<Character> v_crr = factory.makeCarrier(v1_2);
        Ship<Character> h_crr = factory.makeCarrier(h1_2);
        checkShip(v_crr, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));
        checkShip(h_crr, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));

    }

    @Test
    public void test_getCoordinates() {
        Coordinate upperLeft = new Coordinate(1, 1);
        int width = 2;
        int height = 3;
        char data = 's';
        char onHit = '*';
        RectangleShip<Character> ship = new RectangleShip<Character>("submarine", upperLeft, width, height, data, onHit);
        for (Coordinate c : ship.getCoordinates()) {
            assertEquals(data, ship.getDisplayInfoAt(c, true));
        }
    }
}
