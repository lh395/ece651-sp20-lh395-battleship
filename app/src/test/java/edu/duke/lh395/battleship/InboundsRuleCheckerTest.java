package edu.duke.lh395.battleship;

import org.junit.jupiter.api.Test;

public class InboundsRuleCheckerTest {
    @Test
    // (1) We can use our V1ShipFactory for very easy ship creation.
    // (2) Even though we just made our 2 parameter constructor for BattleShipBoard
    //     use InBoundsRuleChecker, we want to explicitly make one here and pass
    //     it into the 3 parameter constructor.  Why?
    //       (a) we don't want to rely on that behavior not changing later,
    //           especially since we explcitly plan to change it!
    //       (b) we want direct access to the InBoundsRuleChecker so we can call
    //           methods on it (our BattleShipBoard doesn't use it yet!)
    public void test_inbounds_rule_checker() {
        V1ShipFactory f = new V1ShipFactory();
        InBoundsRuleChecker<Character> inboundsChecker = new InBoundsRuleChecker<Character>(null);
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, inboundsChecker, 'X');
        Ship<Character> s = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
        b.tryAddShip(s);
    }
}
