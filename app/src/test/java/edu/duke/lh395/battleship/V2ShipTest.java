package edu.duke.lh395.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class V2ShipTest {
    @Test
    public void test_v2_battleship() {
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> battleshipUp = factory.makeBattleship(new Placement(new Coordinate(1, 1), 'U'));
        HashSet<Coordinate> expectedUp = new HashSet<Coordinate>();
        expectedUp.add(new Coordinate(1, 2));
        expectedUp.add(new Coordinate(2, 1));
        expectedUp.add(new Coordinate(2, 2));
        expectedUp.add(new Coordinate(2, 3));
        assertEquals(expectedUp, battleshipUp.getCoordinates());

        Ship<Character> battleshipRight = factory.makeBattleship(new Placement(new Coordinate(1, 1), 'R'));
        HashSet<Coordinate> expectedRight = new HashSet<Coordinate>();
        expectedRight.add(new Coordinate(1, 1));
        expectedRight.add(new Coordinate(2, 1));
        expectedRight.add(new Coordinate(3, 1));
        expectedRight.add(new Coordinate(2, 2));
        assertEquals(expectedRight, battleshipRight.getCoordinates());
    
        Ship<Character> battleshipDown = factory.makeBattleship(new Placement(new Coordinate(1, 1), 'D'));
        HashSet<Coordinate> expectedDown = new HashSet<Coordinate>();
        expectedDown.add(new Coordinate(1, 1));
        expectedDown.add(new Coordinate(1, 2));
        expectedDown.add(new Coordinate(1, 3));
        expectedDown.add(new Coordinate(2, 2));
        assertEquals(expectedDown, battleshipDown.getCoordinates());

        Ship<Character> battleshipLeft = factory.makeBattleship(new Placement(new Coordinate(1, 1), 'L'));
        HashSet<Coordinate> expectedLeft = new HashSet<Coordinate>();
        expectedLeft.add(new Coordinate(2, 1));
        expectedLeft.add(new Coordinate(1, 2));
        expectedLeft.add(new Coordinate(2, 2));
        expectedLeft.add(new Coordinate(3, 2));
        assertEquals(expectedLeft, battleshipLeft.getCoordinates());
    }

    @Test
    public void test_v2_carrier() {
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> carrierUp = factory.makeCarrier(new Placement(new Coordinate(1, 1), 'U'));
        HashSet<Coordinate> expectedUp = new HashSet<Coordinate>();
        expectedUp.add(new Coordinate(1, 1));
        expectedUp.add(new Coordinate(2, 1));
        expectedUp.add(new Coordinate(3, 1));
        expectedUp.add(new Coordinate(4, 1));
        expectedUp.add(new Coordinate(3, 2));
        expectedUp.add(new Coordinate(4, 2));
        expectedUp.add(new Coordinate(5, 2));
        assertEquals(expectedUp, carrierUp.getCoordinates());

        Ship<Character> carrierRight = factory.makeCarrier(new Placement(new Coordinate(1, 1), 'R'));
        HashSet<Coordinate> expectedRight = new HashSet<Coordinate>();
        expectedRight.add(new Coordinate(1, 2));
        expectedRight.add(new Coordinate(1, 3));
        expectedRight.add(new Coordinate(1, 4));
        expectedRight.add(new Coordinate(1, 5));
        expectedRight.add(new Coordinate(2, 1));
        expectedRight.add(new Coordinate(2, 2));
        expectedRight.add(new Coordinate(2, 3));
        assertEquals(expectedRight, carrierRight.getCoordinates());
    
        Ship<Character> carrierDown = factory.makeCarrier(new Placement(new Coordinate(1, 1), 'D'));
        HashSet<Coordinate> expectedDown = new HashSet<Coordinate>();
        expectedDown.add(new Coordinate(1, 1));
        expectedDown.add(new Coordinate(2, 1));
        expectedDown.add(new Coordinate(3, 1));
        expectedDown.add(new Coordinate(2, 2));
        expectedDown.add(new Coordinate(3, 2));
        expectedDown.add(new Coordinate(4, 2));
        expectedDown.add(new Coordinate(5, 2));
        assertEquals(expectedDown, carrierDown.getCoordinates());

        Ship<Character> carrierLeft = factory.makeCarrier(new Placement(new Coordinate(1, 1), 'L'));
        HashSet<Coordinate> expectedLeft = new HashSet<Coordinate>();
        expectedLeft.add(new Coordinate(1, 2));
        expectedLeft.add(new Coordinate(1, 3));
        expectedLeft.add(new Coordinate(1, 4));
        expectedLeft.add(new Coordinate(2, 1));
        expectedLeft.add(new Coordinate(2, 2));
        expectedLeft.add(new Coordinate(2, 3));
        expectedLeft.add(new Coordinate(2, 4));
        assertEquals(expectedLeft, carrierLeft.getCoordinates());

    }
}
