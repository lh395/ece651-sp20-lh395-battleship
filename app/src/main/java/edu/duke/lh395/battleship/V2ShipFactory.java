package edu.duke.lh395.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character> {
    protected Ship<Character> createRectangleShip(Placement where, int w, int h, char letter, String name) throws IllegalArgumentException {
        if (where.getOrientation() != 'H' && where.getOrientation() != 'V') {
            throw new IllegalArgumentException("Orientation must be V(v) or H(h) but is " + where.getCoordinate());
        }
        int width = w;
        int height = h;
        if (where.getOrientation() == 'H') {
            width = h;
            height = w;
        }
        return new RectangleShip<Character>(name, where.getCoordinate(), width, height, letter, '*');
    }

    protected Ship<Character> createV2BattleShip(Placement where, char letter) throws IllegalArgumentException {
        if (where.getOrientation() != 'U' && where.getOrientation() != 'D' && where.getOrientation() != 'L' && where.getOrientation() != 'R') {
            throw new IllegalArgumentException("Orientation must be U(u) or D(d) or L(l) or R(r) but is " + where.getCoordinate());
        }
        return new V2BattleShip<Character>(where.getCoordinate(), where.getOrientation(), letter, '*');
    }

    protected Ship<Character> createV2Carrier(Placement where, char letter) throws IllegalArgumentException {
        if (where.getOrientation() != 'U' && where.getOrientation() != 'D' && where.getOrientation() != 'L' && where.getOrientation() != 'R') {
            throw new IllegalArgumentException("Orientation must be U(u) or D(d) or L(l) or R(r) but is " + where.getCoordinate());
        }
        return new V2Carrier<Character>(where.getCoordinate(), where.getOrientation(), letter, '*');
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createRectangleShip(where, 1, 2, 's', "Submarine");
    }
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createRectangleShip(where, 1, 3, 'd', "Destroyer");
    }
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createV2BattleShip(where, 'b');
    }
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createV2Carrier(where, 'c');
    }
}
