package edu.duke.lh395.battleship;

public class Placement {
    private final Coordinate coordinate;
    private final char orientation;
    public Coordinate getCoordinate() {
        return coordinate;
    }

    public char getOrientation() {
        return orientation;
    }

    /**
     * @description: constructor that takes in both fields of coordinates and orientation and
     * and initializes them appropritately
     * @param c coordinate of newly constructed Placement
     * @param o orientation of newly constructed Placement
     * @return {*}
     */    
    public Placement(Coordinate c, char o) {
        if (o != 'v' && o != 'V' && o != 'h' && o != 'H' && o != 'u' && o != 'U' && o != 'd' && o != 'D' && o != 'l' && o != 'L' && o != 'r' && o != 'R') {
            throw new IllegalArgumentException("Orientation must be V(v) or H(h) or U(u) or D(d) or L(l) or R(r) but is " + o);
        }
        this.coordinate = c;
        this.orientation = Character.toUpperCase(o);
    }

    /**
     * @description: 
     * @param descr {string} is the string description of newly constructed Placement
     * @return {*}
     */
    public Placement(String descr) {
        if (descr.length() != 3) {
            throw new IllegalArgumentException("Placement descr must be 3 chars but is " + descr.length());
        }
        this.coordinate = new Coordinate(descr.substring(0, 2));
        char o = Character.toUpperCase(descr.charAt(2));
        if (o != 'V' && o != 'H' && o != 'U' && o != 'D' && o != 'L' && o != 'R') {
            throw new IllegalArgumentException("Orientation must be V or H or U or D or L or R but is " + o);
        }
        this.orientation = o;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement p = (Placement) o;
            return coordinate.equals(p.coordinate) && orientation == p.orientation;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return "(" + this.coordinate.getRow() + ", " + this.coordinate.getColumn ()+ ", " + this.orientation + ")";
    }
}
