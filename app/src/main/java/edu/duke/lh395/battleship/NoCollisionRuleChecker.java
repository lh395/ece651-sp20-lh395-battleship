package edu.duke.lh395.battleship;

// This class should check the rule that theShip does not collide with anything else
// on theBoard (that all the squares it needs are empty).
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
       Iterable<Coordinate> shipCoords = theShip.getCoordinates();
         for (Coordinate c : shipCoords) {
            if (theBoard.whatIsAtForSelf(c) != null) {
                return "That placement is invalid: the ship overlaps another ship.";
            }
         }
       return null;
    }
}
