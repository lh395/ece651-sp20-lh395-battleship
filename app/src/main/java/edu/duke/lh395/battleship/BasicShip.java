package edu.duke.lh395.battleship;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    protected ArrayList<Coordinate> shipIndex;

    public HashMap<Coordinate, Boolean> getMyPieces() {
        return myPieces;
    }

    public ShipDisplayInfo<T> getMyDisplayInfo() {
        return myDisplayInfo;
    }

    public ShipDisplayInfo<T> getEnemyDisplayInfo() {
        return enemyDisplayInfo;
    }

    public ArrayList<Coordinate> getShipIndex() {
        return shipIndex;
    }

    public void copyInfo(Ship<T> toCopy) {
        for (Coordinate c : toCopy.getShipIndex()) {
            if (toCopy.getMyPieces().get(c)) {
                myPieces.put(shipIndex.get(toCopy.getShipIndex().indexOf(c)), true);
            }
        }
    }

    /**
     * @description: This is a constructor for BasicShip initializes myPieces to
     *               have each coordinate in where as a key and false as the value
     *               and myDisplayInfo to be myDisplayInfo
     * @return {*}
     */
    public BasicShip(Iterable<Coordinate> where, Iterable<Coordinate> index, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        myPieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c : where) {
            myPieces.put(c, false);
        }
        shipIndex = new ArrayList<Coordinate>();
        for (Coordinate c : index) {
            shipIndex.add(c);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }

    /**
     * @description: This method returns true if the coordinate is in myPieces
     * @param {Coordinate} where - the coordinate to check
     * @return {boolean} - indicating whether the coordinate is in myPieces
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    /**
     * @description: This method throws an IllegalArgumentException if the
     *               coordinate is not in myPieces
     * @param {Coordinate} where - the coordinate to check
     * @return {*}
     */
    protected void checkCoordinateInThisShip(Coordinate where) {
        if (!occupiesCoordinates(where)) {
            throw new IllegalArgumentException("Coordinate " + where + " is not in this ship");
        }
    }

    /**
     * @description: This method returns true if all the values in myPieces are true
     * @return {*} - indicating whether the ship is sunk
     */
    @Override
    public boolean isSunk() {
        for (Coordinate c : myPieces.keySet()) {
            if (!myPieces.get(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @description: This method sets the value of the coordinate in myPieces to be
     *               true if the coordinate is in myPieces
     * @param {Coordinate} where - the coordinate to check
     * @return {*}
     */
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    /**
     * @description: This method returns true if the value of the coordinate in
     *               myPieces is true
     * @param {Coordinate} where
     * @return {boolean} - indicating whether the coordinate is hit
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        if (myPieces.get(where)) {
            return true;
        }
        return false;
    }

    /**
     * @description: This method returns the display info at the coordinate
     * @return {T} - the display info at the coordinate
     */    
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);
        if (myShip) {
            return myDisplayInfo.getInfo(where, myPieces.get(where));
        } else {
            return enemyDisplayInfo.getInfo(where, myPieces.get(where));
        }
    }

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

}
