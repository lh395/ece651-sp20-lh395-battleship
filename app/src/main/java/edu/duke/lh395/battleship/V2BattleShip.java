package edu.duke.lh395.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class V2BattleShip<T> extends BasicShip<T> {
    private final String name = "Battleship";

    public String getName() {
        return name;
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
        HashSet<Coordinate> coordinates = new HashSet<Coordinate>();
        ArrayList<Coordinate> index = makeIndex(upperLeft, orientation);
        for (Coordinate c : index) {
            coordinates.add(c);
        }
        return coordinates;
    }

    public static ArrayList<Coordinate> makeIndex(Coordinate upperLeft, char orientation) {
        ArrayList<Coordinate> index = new ArrayList<Coordinate>();
        if (orientation == 'U') {
            index.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
            for (int i = 0; i < 3; i++) {
                index.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
            }
        } else if (orientation == 'R') {
            index.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
            for (int i = 0; i < 3; i++) {
                index.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
            }
        } else if (orientation == 'D') {
            index.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
            for (int i = 0; i < 3; i++) {
                index.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + i));
            }
        } else if (orientation == 'L') {
            index.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
            for (int i = 0; i < 3; i++) {
                index.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + 1));
            }
        }
        return index;
    }

    public V2BattleShip(Coordinate upperLeft, char orientation, ShipDisplayInfo<T> mydisplayInfo, ShipDisplayInfo<T> enemydisplayInfo) {
        super(makeCoords(upperLeft, orientation), makeIndex(upperLeft, orientation), mydisplayInfo, enemydisplayInfo);
    }

    public V2BattleShip(Coordinate upperLeft, char orientation, T data, T onHit) {
        this(upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }
}
