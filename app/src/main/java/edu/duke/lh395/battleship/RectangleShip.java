package edu.duke.lh395.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    private final String name;

    public String getName() {
        return name;
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> coordinates = new HashSet<Coordinate>();
        ArrayList<Coordinate> index = makeIndex(upperLeft, width, height);
        for (Coordinate c : index) {
            coordinates.add(c);
        }
        return coordinates;
    }

    public static ArrayList<Coordinate> makeIndex(Coordinate upperLeft, int width, int height) {
        ArrayList<Coordinate> index = new ArrayList<Coordinate>();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                index.add(new Coordinate(upperLeft.getRow() + j, upperLeft.getColumn() + i));
            }
        }
        return index;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> mydisplayInfo, ShipDisplayInfo<T> enemydisplayInfo) {
        super(makeCoords(upperLeft, width, height), makeIndex(upperLeft, width, height), mydisplayInfo, enemydisplayInfo);
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }
}
