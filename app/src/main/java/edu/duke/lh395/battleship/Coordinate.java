package edu.duke.lh395.battleship;

public class Coordinate {
    private final int row;
    private final int column;

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    /**
     * Constructs a Coordinate with the specified row and column
     * 
     * @param r is the row of the newly constructed coordinate.
     * @param c is the column of the newly constructed coordinate.
     */
    public Coordinate(int r, int c) {
        this.row = r;
        this.column = c;
    }

    /**
     * Constructs a Coordinate with the specified row and column from description
     * 
     * @param descr is the description (row and column) of the newly constructed coordinate.
     */
    public Coordinate(String descr) {
        if (descr.length() != 2) {
            throw new IllegalArgumentException("Coordinate descr must be 2 chars but is " + descr.length());
        }
        String upDescr = descr.toUpperCase();
        char rowLetter = upDescr.charAt(0);
        int column = Character.getNumericValue(upDescr.charAt(1));
        if (rowLetter < 'A' || rowLetter > 'Z') {
            throw new IllegalArgumentException("Coordinate row must be letter between A and Z but is " + rowLetter);
        }
        if (column < 0 || column > 9) {
            throw new IllegalArgumentException("Coordinate column must be letter between 0 and 9 but is " + column);
        }
        this.row = rowLetter - 'A';
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return "(" + row + ", " + column+")";
    }
}
