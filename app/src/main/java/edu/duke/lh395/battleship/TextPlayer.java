package edu.duke.lh395.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class TextPlayer {
    private final Board<Character> theBoard;
    private final BoardTextView view;
    private final BufferedReader inputReader;
    private final PrintStream out;
    private final AbstractShipFactory<Character> shipFactory;
    private String name;
    private final ArrayList<String> shipsToPlace;
    private final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    protected int moveTimes;
    protected int sonarTimes;

    public Board<Character> getTheBoard() {
        return theBoard;
    }

    public String getName() {
        return name;
    }

    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /**
     * @description: This method is used to initialize the TextPlayer class
     * @return {*}
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.name = name;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationMap();
        setupShipCreationList();
        this.moveTimes = 2;
        this.sonarTimes = 1;
    }

    /**
     * @description: This method is used to read one placement from the user
     * @return {*}
     */
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new IOException("Unexpected end of input");
        }
        return new Placement(s);
    }

    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new IOException("Unexpected end of input");
        }
        return new Coordinate(s);
    }

    /**
     * @description: This method is used to do one placement for the user
     * @return {*}
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        String problem = null;
        do {
            if (problem != null) {
                out.println(problem);
                out.println(view.displayMyOwnBoard());
            }
            try {
                Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                problem = theBoard.tryAddShip(s);
            } catch (IllegalArgumentException e) {
                problem = e.getMessage();
            }
        } while (problem != null);
        out.println(view.displayMyOwnBoard());
      }

    public boolean doPlaecmentPhase() throws IOException {
        out.println("Welcome to Battleship, " + name + "!");
        String problem = null;
        do {
            if (problem != null) {
                out.println(problem);
            }
            try {
                out.println("Would you like to play as a computer? (y/n)");
                String s = inputReader.readLine().toLowerCase();
                if(s.equals("y")){
                    computerPlace();
                    return true;
                } else if(s.equals("n")){
                    problem = null;
                } else {
                    throw new IllegalArgumentException("Please enter y or n");
                }
            } catch (IllegalArgumentException e) {
                problem = e.getMessage();
            }
        } while (problem != null);

        out.println(view.displayMyOwnBoard());
        String s = "Player " + name + ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6";
        out.println(s);
        for (String ship:shipsToPlace) {
            doOnePlacement(ship, shipCreationFns.get(ship));
        }
        return false;
    }

    private void doOneFire(Board<Character> enemyBoard, boolean isComputer) throws IOException {
        String problem = null;
        do {
            if (problem != null) {
                out.println(problem);
            }
            try {
                Coordinate c;
                if (isComputer) {
                    Random rand = new Random();
                    int row = rand.nextInt(theBoard.getHeight());
                    int col = rand.nextInt(theBoard.getWidth());
                    c = new Coordinate(row, col);
                } else {
                    c = readCoordinate("Player " + name + " where do you want to attack?");
                }
                enemyBoard.fireAt(c);
                Character res = enemyBoard.whatIsAtForEnemy(c);
                if (res == 's') {
                    out.println("You hit a submarine!");
                } else if (res == 'd') {
                    out.println("You hit a destroyer!");
                } else if (res == 'b') {
                    out.println("You hit a battleship!");
                } else if (res == 'c') {
                    out.println("You hit a carrier!");
                } else {
                    out.println("You missed!");
                }
                problem = null;
            }   catch (IllegalArgumentException e) {
                problem = e.getMessage();
            }
        } while (problem != null);
    }

    /**
     * @description: This method is used to get the ship of the given coordinate
     * @return {*}
     */    
    private Ship<Character> getShip(Coordinate c) {
        for (Ship<Character> s : theBoard.getMyShips()) {
            if (s.getMyPieces().containsKey(c)) {
                return s;
            }
        }
        return null;
    }

    private void doOneMove() throws IOException {
        Ship<Character> toMove = null;
        String problem = null;
        do {
            if (problem != null) {
                out.println(problem);
            }
            try {
                Coordinate c = readCoordinate("Player " + name + " which ship do you want to move?");
                toMove = getShip(c);
                if (toMove == null) {
                    throw new IllegalArgumentException("There is no ship at the given coordinate!");
                } else {
                    Placement p = readPlacement("Player " + name + " where do you want to move the ship?");
                    theBoard.moveShip(toMove, p);
                    problem = null;
                }
            } catch (IllegalArgumentException e) {
                problem = e.getMessage();
            }
        } while (problem != null);
    }

    private void doOneSonar(Board<Character> enemyBoard) throws IOException {
        Coordinate c = null;
        String problem = null;
        HashMap<String, Integer> sonarMap = null;
        do {
            if (problem != null) {
                out.println(problem);
            }
            try {
                c = readCoordinate("Player " + name + " where do you want to sonar scan?");
                if (c.getColumn() < 0 || c.getColumn() > theBoard.getWidth() || c.getRow() < 0 || c.getRow() > theBoard.getHeight()) {
                    throw new IllegalArgumentException("The coordinate is out of the board!");
                }
                sonarMap = enemyBoard.scan(c);
                if (sonarMap != null) {
                    out.println("Sonar scan result:");
                    out.println("There are " + sonarMap.get("Submarine") + " square(s) that occupe by submarines");
                    out.println("There are " + sonarMap.get("Destroyer") + " square(s) that occupe by destroyers");
                    out.println("There are " + sonarMap.get("Battleship") + " square(s) that occupe by battleships");
                    out.println("There are " + sonarMap.get("Carrier") + " square(s) that occupe by carriers");
                } else {
                    throw new IllegalArgumentException("Scan Failed");
                }
                problem = null;
            } catch (IllegalArgumentException e) {
                problem = e.getMessage();
            }
        } while (problem != null);
    }

    public void playOneTurn(Board<Character> enemyBoard, String enemyName, boolean isComputer) throws IOException, IllegalArgumentException {
        String problem = null;
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        out.println("Player " + name + "'s turn:\n");
        out.println(view.displayMyBoardWithEnemyNextToIt(enemyView, "Your Ocean", "Player " + enemyName + "'s Ocean\n"));
        String option;
        do {
            if (problem != null) {
                out.println(problem);
            }
            try {
                if (isComputer) {
                    doOneFire(enemyBoard, isComputer);
                    problem = null;
                    continue;
                }
                if (moveTimes > 0 || sonarTimes > 0) {
                    if (moveTimes > 0 && sonarTimes >0) {
                        out.println("Possible actions for Player " + name + " :\n" +
                        " F Fire at a square\n" +
                        " M Move a ship to another square (" + moveTimes + " remaining)\n" +
                        " S Sonar scan (" + sonarTimes + " remaining)\n" +
                        "Player " + name + ", what would you like to do?");
                    } else if (moveTimes > 0) {
                        out.println("Possible actions for Player " + name + " :\n" +
                        " F Fire at a square\n" +
                        " M Move a ship to another square (" + moveTimes + " remaining)\n" +
                        "Player " + name + ", what would you like to do?");
                    } else if (sonarTimes > 0) {
                        out.println("Possible actions for Player " + name + " :\n" +
                        " F Fire at a square\n" +
                        " S Sonar scan (" + sonarTimes + " remaining)\n" +
                        "Player " + name + ", what would you like to do?");
                    }
                    option = inputReader.readLine().toUpperCase();
                    if (option.equals("F")) {
                        doOneFire(enemyBoard, isComputer);
                        problem = null;
                    } else if (option.equals("M") && moveTimes > 0) {
                        doOneMove();
                        moveTimes--;
                        problem = null;
                    } else if (option.equals("S") && sonarTimes > 0) {
                        doOneSonar(enemyBoard);
                        sonarTimes--;
                        problem = null;
                    } else {
                        throw new IllegalArgumentException("Invalid option, please try again");
                    }
                } else {
                    doOneFire(enemyBoard, isComputer);
                    problem = null;
                }
            } catch (Exception e) {
                problem = e.getMessage();
            }
        } while (problem != null);
    }

    public void computerPlace() {
        Ship<Character> sh1 = shipFactory.makeSubmarine(new Placement("a0v"));
        Ship<Character> sh2 = shipFactory.makeSubmarine(new Placement("c0v"));
        Ship<Character> sh3 = shipFactory.makeDestroyer(new Placement("e4h"));
        Ship<Character> sh4 = shipFactory.makeDestroyer(new Placement("b3v"));
        Ship<Character> sh5 = shipFactory.makeDestroyer(new Placement("b4h"));
        Ship<Character> sh6 = shipFactory.makeBattleship(new Placement("f2u"));
        Ship<Character> sh7 = shipFactory.makeBattleship(new Placement("f7u"));
        Ship<Character> sh8 = shipFactory.makeBattleship(new Placement("i2d"));
        Ship<Character> sh9 = shipFactory.makeCarrier(new Placement("i7d"));
        Ship<Character> sh10 = shipFactory.makeCarrier(new Placement("l5u"));
        theBoard.tryAddShip(sh1);
        theBoard.tryAddShip(sh2);
        theBoard.tryAddShip(sh3);
        theBoard.tryAddShip(sh4);
        theBoard.tryAddShip(sh5);
        theBoard.tryAddShip(sh6);
        theBoard.tryAddShip(sh7);
        theBoard.tryAddShip(sh8);
        theBoard.tryAddShip(sh9);
        theBoard.tryAddShip(sh10);
        return;
    }

    public void computerAttack(Board<Character> enemyBoard) {
        Random rand = new Random();
        int row = rand.nextInt(theBoard.getHeight());
        int col = rand.nextInt(theBoard.getWidth());
        Coordinate c = new Coordinate(row, col);
        enemyBoard.fireAt(c);
    }

    public boolean lose() {
        return theBoard.allSunk();
    }
}
