package edu.duke.lh395.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class V2Carrier<T> extends BasicShip<T> {
    private final String name = "Carrier";
    public String getName() {
        return name;
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
        HashSet<Coordinate> coordinates = new HashSet<Coordinate>();
        ArrayList<Coordinate> index = makeIndex(upperLeft, orientation);
        for (Coordinate c : index) {
            coordinates.add(c);
        }
        return coordinates;
    }

    public static ArrayList<Coordinate> makeIndex(Coordinate upperLeft, char orientation) {
        ArrayList<Coordinate> index = new ArrayList<Coordinate>();
        if (orientation == 'U') {
            for (int i = 0; i < 4; i++) {
                index.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
            }
            for (int i = 2; i < 5; i++) {
                index.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + 1));
            }
        } else if (orientation == 'R') {
            for (int i = 0; i < 3; i++) {
                index.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
            }
            for (int i = 1; i < 5; i++) {
                index.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + i));
            }
        } else if (orientation == 'D') {
            for (int i = 0; i < 3; i++) {
                index.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
            }
            for (int i = 1; i < 5; i++) {
                index.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + 1));
            }
        } else if (orientation == 'L') {
            for (int i = 0; i < 4; i++) {
                index.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
            }
            for (int i = 1; i < 4; i++) {
                index.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + i));
            }
        }
        return index;
    }

    public V2Carrier(Coordinate upperLeft, char orientation, ShipDisplayInfo<T> mydisplayInfo, ShipDisplayInfo<T> enemydisplayInfo) {
        super(makeCoords(upperLeft, orientation), makeIndex(upperLeft, orientation), mydisplayInfo, enemydisplayInfo);
    }

    public V2Carrier(Coordinate upperLeft, char orientation, T data, T onHit) {
        this(upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }


}
