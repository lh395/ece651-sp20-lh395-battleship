package edu.duke.lh395.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board it will display.
     * 
     * @param toDisplay is the Board to display
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * 
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep = ""; // start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * This display the given board including both header and body
     * 
     * @return the String that is the given board
     */
    public String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String header = makeHeader();
        StringBuilder body = new StringBuilder("");
        for (int row = 0; row < toDisplay.getHeight(); row++) {
            body.append((char)(row % 26 + (int) 'A') + " ");
            for (int column = 0; column < toDisplay.getWidth(); column++) {
                if (column > 0) {
                    body.append("|");
                }
                Character c = getSquareFn.apply(new Coordinate(row, column));
                if (c == null) {
                    body.append(" ");
                } else {
                    body.append(c);
                }
            }
            body.append(" " + (char)(row % 26 + (int) 'A'));
            body.append("\n");
        }
        String ans = header + body + header;
        return ans.toString();
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
    }

    public String displayEnemyBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String [] myLines = displayMyOwnBoard().split("\n");
        String [] enemyLines = enemyView.displayEnemyBoard().split("\n");
        if (myLines.length != enemyLines.length) {
            throw new IllegalArgumentException("Boards must have same number of rows");
        }
        int enemyHeaderStart = toDisplay.getWidth() * 2 + 22;;
        int enemyBoardStart = toDisplay.getWidth() * 2 + 19;
        StringBuilder ans = new StringBuilder();
        ans.append(" ".repeat(5) + myHeader + " ".repeat(enemyHeaderStart - myHeader.length() -5) + enemyHeader + "\n");
        for (int i = 0; i < myLines.length; i++) {
            ans.append(myLines[i] + " ".repeat(enemyBoardStart - myLines[i].length()) + enemyLines[i] + "\n");
        }
        return ans.toString();
    }
}
