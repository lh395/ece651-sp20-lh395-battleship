package edu.duke.lh395.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
    private int width;
    private int height;
    private final ArrayList<Ship<T>> myShips;
    private final HashSet<Coordinate> enemyMisses;
    private final T missInfo;
    protected HashMap<Coordinate, Character> hitRecord;
    
    public ArrayList<Ship<T>> getMyShips() {
        return myShips;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String tryAddShip(Ship<T> toAdd) {
        String placementProblem = placementChecker.checkPlacement(toAdd, this);
        if (placementProblem == null) {
            myShips.add(toAdd);
            return null;
        }
        return placementProblem;
    }

    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(c)) {
                s.recordHitAt(c);
                if (!hitRecord.containsKey(c)) {
                    hitRecord.put(c, Character.toLowerCase(s.getName().charAt(0)));
                }
                for (Coordinate miss : enemyMisses) {
                    if (miss.equals(c)) {
                        enemyMisses.remove(miss);
                        break;
                    }
                }
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }

    private final PlacementRuleChecker<T> placementChecker;

    /**
     * @description: This method takes a Coordinate and sees which (if any) Ship
     *               occupies that Coordinate.
     * @param {Coordinate} where : The Coordinate to check
     * @return {*} : The displayInfo of the Ship that occupies the Coordinate, or
     *         null if no Ship occupies the Coordinate.
     */
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(where)) {
                if (isSelf || (hitRecord.containsKey(where) && !enemyMisses.contains(where))) {
                    return s.getDisplayInfoAt(where, isSelf);
                } 
            }
        }
        if (!isSelf && enemyMisses.contains(where)) {
            return missInfo;
        }
        if (!isSelf && hitRecord.containsKey(where)) {
            return (T) hitRecord.get(where);
        }
        return null;
    }

    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    public boolean allSunk() {
        for (Ship<T> s : myShips) {
            if (!s.isSunk()) {
                return false;
            }
        }
        return true;
    }


    public void moveShip(Ship<T> ship, Placement newPlacement) throws IllegalArgumentException {
        int index = myShips.indexOf(ship);
        Ship<T> toMove = ship;
        myShips.remove(ship);
        V2ShipFactory f = new V2ShipFactory();
        Ship<Character> newShip = null;
        if (toMove.getName() == "Submarine") {
            newShip = f.makeSubmarine(newPlacement);
        } else if (toMove.getName() == "Destroyer") {
            newShip = f.makeDestroyer(newPlacement);
        } else if (toMove.getName() == "Battleship") {
            newShip = f.makeBattleship(newPlacement);
        } else if (toMove.getName() == "Carrier") {
            newShip = f.makeCarrier(newPlacement);
        }
        newShip.copyInfo((Ship<Character>) toMove);
        String problem = placementChecker.checkPlacement((Ship<T>) newShip, this);
        if (problem == null) {
            myShips.add(index, (Ship<T>) newShip);
        } else {
            myShips.add(index, toMove); // recovery
            throw new IllegalArgumentException(problem);
        }
    }

    public HashMap<String, Integer> scan(Coordinate c) {
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        int submarineCount = 0;
        int destroyerCount = 0;
        int battleshipCount = 0;
        int carrierCount = 0;
        for (int i = c.getRow() - 3; i <= c.getRow() + 3; i++){
            for (int j = c.getColumn() - 3; j <= c.getColumn() + 3; j++){
                if(Math.abs(i - c.getRow()) + Math.abs(j - c.getColumn()) <= 3) {
                    if ((i >= 0 && i < height) && (j >= 0  && j < width)) {
                        for (Ship<T> ship : myShips) {
                            if (ship.occupiesCoordinates(new Coordinate(i, j))) {
                                if (ship.getName() == "Submarine") {
                                    submarineCount++;
                                } else if (ship.getName() == "Destroyer") {
                                    destroyerCount++;
                                } else if (ship.getName() == "Battleship") {
                                    battleshipCount++;
                                } else if (ship.getName() == "Carrier") {
                                    carrierCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
        result.put("Submarine", submarineCount);
        result.put("Destroyer", destroyerCount);
        result.put("Battleship", battleshipCount);
        result.put("Carrier", carrierCount);
        return result;
    }

    /**
     * Constructs a BattleShipBoard with the specified width and height
     * 
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or
     *                                  equal to zero.
     */
    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementChecker, T missInfo) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<Coordinate>();
        this.missInfo = missInfo;
        this.hitRecord = new HashMap<Coordinate, Character>();
    }

    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)), missInfo);
    }


}
