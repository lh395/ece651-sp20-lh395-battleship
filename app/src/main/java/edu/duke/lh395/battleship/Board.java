package edu.duke.lh395.battleship;

import java.util.ArrayList;
import java.util.HashMap;

public interface Board<T> {
    public int getWidth();
    public int getHeight();
    public ArrayList<Ship<T>> getMyShips();
    public String tryAddShip(Ship<T> toAdd);
    public T whatIsAtForSelf(Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);
    public Ship<T> fireAt(Coordinate c);
    public boolean allSunk();
    public HashMap<String, Integer> scan(Coordinate c);
    public void moveShip(Ship<T> s, Placement p);
}
