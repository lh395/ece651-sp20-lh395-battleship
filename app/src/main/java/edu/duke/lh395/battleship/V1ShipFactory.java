package edu.duke.lh395.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) throws IllegalArgumentException {
        if (where.getOrientation() != 'H' && where.getOrientation() != 'V') {
            throw new IllegalArgumentException("Orientation must be V(v) or H(h) but is " + where.getCoordinate());
        }
        int width = w;
        int height = h;
        if (where.getOrientation() == 'H') {
            width = h;
            height = w;
        }
        return new RectangleShip<Character>(name, where.getCoordinate(), width, height, letter, '*');
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

}
