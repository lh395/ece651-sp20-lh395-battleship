package edu.duke.lh395.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
      }

    // All this need to do is iterate over all the coordinates in theShip and check that they are
    // in bounds on theBoard (i.e. 0 <= row < height and 0<= column < width).
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
       Iterable<Coordinate> shipCoords = theShip.getCoordinates();
         for (Coordinate c : shipCoords) {
            if (c.getRow() < 0) {
              return "That placement is invalid: the ship goes off the top of the board.";
            } else if(c.getRow() >= theBoard.getHeight()) {
              return "That placement is invalid: the ship goes off the bottom of the board.";
            } else if (c.getColumn() < 0) {
              return "That placement is invalid: the ship goes off the left of the board.";
            } else if (c.getColumn() >= theBoard.getWidth()) {
              return "That placement is invalid: the ship goes off the right of the board.";
            }
         }
       return null;
    }
}
